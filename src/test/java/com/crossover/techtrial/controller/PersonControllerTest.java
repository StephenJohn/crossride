/**
 * 
 */
package com.crossover.techtrial.controller;

import com.crossover.techtrial.exceptions.CrossRideException;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author John Stephen Komna
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PersonControllerTest {
  
  MockMvc mockMvc;
  
  @Mock
  private PersonController personController;
  
  @Autowired
  private TestRestTemplate template;
  
  @Autowired
  PersonRepository personRepository;

  @Before
  public void setup() throws CrossRideException {
    mockMvc = MockMvcBuilders.standaloneSetup(personController).build();
  }
  

  @Test
  public void getAllPersons() throws CrossRideException {
      JSONObject personJson = new JSONObject();
      personJson.put("name", "test 1");
      personJson.put("email", "test10000000000001@gmail.com");
      personJson.put("designation", 1);
      personJson.put("registrationNumber", "41DCT");
      personJson.put("createdDate", "2018-08-08T12:12:12");
      ResponseEntity<Person> responseRegistered= register(personJson);

      ResponseEntity<Person[]>  response = template.getForEntity(
              "/api/person",  Person[].class);
      //Delete this user
      personRepository.deleteById(responseRegistered.getBody().getId());
      Assert.assertFalse(response.getBody().length==0);
      Assert.assertNotNull(response.getBody());
      Assert.assertEquals(200,response.getStatusCode().value());
  }

    @Test
    public void getAllDrivers() throws CrossRideException {
        JSONObject personJson = new JSONObject();
        personJson.put("name", "test 1");
        personJson.put("email", "test10000000000001@gmail.com");
        personJson.put("designation", 1);
        personJson.put("registrationNumber", "41DCT");
        personJson.put("createdDate", "2018-08-08T12:12:12");
        ResponseEntity<Person> responseRegistered= register(personJson);
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("designation", 1L);
        ResponseEntity<Person[]>  response = template.getForEntity(
                "/api/personByDesignation/{designation}",  Person[].class, map);
        //Delete this user
        personRepository.deleteById(responseRegistered.getBody().getId());
        Assert.assertFalse(response.getBody().length==0);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(200,response.getStatusCode().value());
    }


    @Test
  public void getPersonById() throws CrossRideException {

      JSONObject personJson = new JSONObject();
      personJson.put("name", "test 1");
      personJson.put("email", "test10000000000001@gmail.com");
      personJson.put("designation", 1);
      personJson.put("registrationNumber", "41DCT");
      personJson.put("createdDate", "2018-08-08T12:12:12");
      ResponseEntity<Person> responsePersonRegistered = register(personJson);

      Map<String, Long> map = new HashMap<String, Long>();
      map.put("personId", responsePersonRegistered.getBody().getId());
      ResponseEntity<Person> getResponse = template.getForEntity(
              "/api/person/{personId}",  Person.class, map);
      //Delete this user
      personRepository.deleteById(getResponse.getBody().getId());
      Assert.assertEquals("test 1", getResponse.getBody().getName());
      Assert.assertEquals(200,getResponse.getStatusCode().value());

  }
  
  @Test
  public void testPanelShouldBeRegistered() throws CrossRideException {
      JSONObject personJson = new JSONObject();
      personJson.put("name", "test 1");
      personJson.put("email", "test10000000000001@gmail.com");
      personJson.put("designation", 1);
      personJson.put("registrationNumber", "41DCT");
      personJson.put("createdDate", "2018-08-08T12:12:12");
      ResponseEntity<Person> response= register(personJson);
      //Delete this user
      personRepository.deleteById(response.getBody().getId());
      Assert.assertEquals("test 1", response.getBody().getName());
      Assert.assertEquals(200,response.getStatusCode().value());
  }

    public ResponseEntity<Person> register(JSONObject personJson) throws CrossRideException {

        HttpEntity<Object> person = getHttpEntity(personJson);
        ResponseEntity<Person> response = template.postForEntity(
                "/api/person", person, Person.class);
        return response;
    }

  private HttpEntity<Object> getHttpEntity(Object body) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<Object>(body, headers);
  }

}
