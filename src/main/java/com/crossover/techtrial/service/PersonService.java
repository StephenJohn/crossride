/**
 * 
 */
package com.crossover.techtrial.service;

import java.util.List;
import com.crossover.techtrial.model.Person;

/**
 * PersonService interface for Persons.
 * @author cossover
 *
 */
public interface PersonService {
  public List<Person> getAll();
  
  public Person save(Person p);
  public List<Person> findByDesignation(int designation);
  public Person findById(Long personId);
  
}
