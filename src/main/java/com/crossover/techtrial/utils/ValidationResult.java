package com.crossover.techtrial.utils;

public class ValidationResult {
    private String message;
    private boolean isValid;

    public ValidationResult(String message, boolean isValid)
    {
        this.message = message;
        this.isValid = isValid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
}
