package com.crossover.techtrial.utils;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.service.PersonService;

import java.time.LocalDateTime;

public class Validations {
    // Method to validate Person before registration
    public static ValidationResult isPersonValid(Person person)
    {
    // Check if Person Designature is Set and Valid
    if (person.getDesignation() !=PersonDesignation.DRIVER && person.getDesignation() != PersonDesignation.RIDER) {
        return new ValidationResult("Invalid Designation ",false);
    }

    if (person.getName() == null || person.getName().equals("")) {
        return new ValidationResult("Invalid Person Name ",false);
    }
        return new ValidationResult("Ok",true);
    }

    // Method to Validate Ride before Creating New Ride
    public static ValidationResult isRideValid(Ride ride, PersonService personService)
    {
            //Check if Driver Exist
            if (personService.findById(ride.getDriver().getId())==null) {
                return new ValidationResult("Driver does not Exist ",false);
            }

            //Check if Rider Exist
            if (personService.findById(ride.getRider().getId())==null) {
                return new ValidationResult("Rider does not Exist ",false);
            }

            //Check if EndTime is Bigger than and not Equal to StartTime
            if (ride.getStartTime().isAfter(ride.getEndTime()) || ride.getStartTime().isEqual(ride.getEndTime())) {
                return new ValidationResult("Start Time Cannot be Equal or Bigger than End time ",false);
            }


        return new ValidationResult("Ok ",true);

    }


    public static ValidationResult isTop5DriverRequestPayLoadValid(LocalDateTime startTime, LocalDateTime endTime)
    {
        if (startTime.isAfter(endTime) || startTime.isEqual(endTime)) {
            return new ValidationResult("Start Time Cannot be Equal or Bigger than End time ",false);
        }
        else
        {
            return new ValidationResult("Ok ",true);
        }
    }

    public static ValidationResult isDesignationValid(int designation)
    {
        if (designation != PersonDesignation.DRIVER &&
                designation != PersonDesignation.RIDER) {
            return new ValidationResult("Invalid Designation {DRIVER:1 RIDER:2} ",false);
        }
        else
        {
            return new ValidationResult("Ok ",true);
        }
    }

}
