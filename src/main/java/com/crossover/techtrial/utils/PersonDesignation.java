package com.crossover.techtrial.utils;

// Enum for Identifying the Designation of Person
public class PersonDesignation {
   public static int DRIVER =1;
   public static int RIDER =2;
}
