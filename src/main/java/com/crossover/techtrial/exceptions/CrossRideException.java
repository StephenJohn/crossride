package com.crossover.techtrial.exceptions;

import org.springframework.http.HttpStatus;

public class CrossRideException extends RuntimeException {
     private HttpStatus httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;

   public CrossRideException(String message,HttpStatus httpStatus)
    {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus()
    {
        return this.httpStatus;
    }
}
