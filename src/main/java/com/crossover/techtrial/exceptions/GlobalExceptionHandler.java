package com.crossover.techtrial.exceptions;

import java.util.AbstractMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Component
public class GlobalExceptionHandler extends Exception {

  private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  /**
   * Global Exception handler for all exceptions.
   */
  @ExceptionHandler(CrossRideException.class)
  public ResponseEntity handle(CrossRideException exception) {
    // general exception
    LOG.error("Exception: Unable to process this request. ", exception);
    return ResponseEntity.status(exception.getHttpStatus()).body(exception.getMessage());
  }
}
