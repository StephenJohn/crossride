/**
 * 
 */
package com.crossover.techtrial.repositories;

import com.crossover.techtrial.model.Ride;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import com.crossover.techtrial.dto.TopDriverDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author crossover
 *
 */
@RestResource(exported = false)
public interface RideRepository extends CrudRepository<Ride, Long> {
    @Query("select new com.crossover.techtrial.dto.TopDriverDTO(r.driver.name, r.driver.email, " +
            "sum(r.duration), max(r.duration), avg(r.distance)) from Ride r where r.startTime >= :startTime and r.endTime <= :endTime" +
            " group by r.driver order by max(r.duration) desc ")
    List<TopDriverDTO> TopDriver(
            @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);
}
