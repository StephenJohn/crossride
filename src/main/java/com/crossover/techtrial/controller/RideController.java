/**
 * 
 */
package com.crossover.techtrial.controller;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.crossover.techtrial.exceptions.CrossRideException;
import com.crossover.techtrial.service.PersonService;
import com.crossover.techtrial.utils.ValidationResult;
import com.crossover.techtrial.utils.Validations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.service.RideService;

/**
 * RideController for Ride related APIs.
 * @author crossover
 *
 */
@RestController
public class RideController {
  
  @Autowired
  RideService rideService;

  @Autowired
  PersonService personService;

  @PostMapping(path ="/api/ride")
  public ResponseEntity<Ride> createNewRide(@RequestBody Ride ride) {
    try
    {
      ValidationResult validationResult = Validations.isRideValid(ride,personService);
      if(validationResult.isValid()==true)
      {
      long duration =  Duration.between(ride.getStartTime(),ride.getEndTime()).toMinutes();
      ride.setDuration(duration);
      return ResponseEntity.ok(rideService.save(ride));
      }
      else
      {
        throw new CrossRideException(validationResult.getMessage(), HttpStatus.BAD_REQUEST);
          }
    }
    catch (Exception ex)
    {
      throw new CrossRideException("An Error Occured", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @GetMapping(path = "/api/ride/{ride-id}")
  public ResponseEntity<Ride> getRideById(@PathVariable(name="ride-id",required=true)Long rideId) {
    try {
      Ride ride = rideService.findById(rideId);
      if (ride != null) {
        return ResponseEntity.ok(ride);
      } else {
        throw new CrossRideException("Ride not Found", HttpStatus.NOT_FOUND);
      }
    } catch (Exception ex) {
      throw new CrossRideException("An Error Occured", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  /**
   * This API returns the top 5 drivers with their email,name, total minutes, maximum ride duration in minutes.
   * Only rides that starts and ends within the mentioned durations should be counted.
   * Any rides where either start or endtime is outside the search, should not be considered.
   * 
   * DONT CHANGE METHOD SIGNATURE AND RETURN TYPES
   * @return
   */
  @GetMapping(path = "/api/top-rides")
  public ResponseEntity<List<TopDriverDTO>> getTopDriver(
      @RequestParam(value="max", defaultValue="5") Long count,
      @RequestParam(value="startTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime startTime,
      @RequestParam(value="endTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime endTime){
    List<TopDriverDTO> topDrivers = new ArrayList<TopDriverDTO>();
    try {
      /**
       * Your Implementation Here. And Fill up topDrivers Arraylist with Top
       *
       */
      //validate RequestPayLoad
      ValidationResult validationResult = Validations.isTop5DriverRequestPayLoadValid(startTime,endTime);
       if(validationResult.isValid()==true)
       {
         topDrivers = rideService.TopDriver(startTime,endTime);
         return ResponseEntity.ok(topDrivers);
       }
       else
       {
         throw new CrossRideException(validationResult.getMessage(), HttpStatus.BAD_REQUEST);
       }
    }
    catch (Exception ex) {
    }
    throw new CrossRideException("An Error Occured", HttpStatus.INTERNAL_SERVER_ERROR);
    }


  
}
