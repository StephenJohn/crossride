/**
 * 
 */
package com.crossover.techtrial.controller;

import java.util.List;

import com.crossover.techtrial.exceptions.CrossRideException;
import com.crossover.techtrial.utils.PersonDesignation;
import com.crossover.techtrial.utils.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.service.PersonService;
import com.crossover.techtrial.utils.Validations;

/**
 * 
 * @author crossover
 */

@RestController
public class PersonController {
  
  @Autowired
  PersonService personService;
  
  @PostMapping(path = "/api/person")
  public ResponseEntity<Person> register(@RequestBody Person person) {


    try {
      ValidationResult validationResult = Validations.isPersonValid(person);
      if (validationResult.isValid() == false) {
        throw new CrossRideException(validationResult.getMessage(), HttpStatus.BAD_REQUEST);
      }
      return ResponseEntity.ok(personService.save(person));
    }
    catch(Exception ex)
    {
      throw new CrossRideException("An Error Occured ", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @GetMapping(path = "/api/person")
  public ResponseEntity<List<Person>> getAllPersons() {
    try {
      return ResponseEntity.ok(personService.getAll());
    }
    catch(Exception ex)
    {
      throw new CrossRideException("An Error Occured", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @GetMapping(path = "/api/person/{personId}")
  public ResponseEntity<Person> getPersonById(@PathVariable(name="personId", required=true)Long personId) {
    try {
      Person person = personService.findById(personId);
      if (person != null) {
        return ResponseEntity.ok(person);
      } else {
        throw new CrossRideException("No Person was Found ", HttpStatus.NOT_FOUND);
      }
    } catch (Exception ex) {
      throw new CrossRideException("An Error occured", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // Get Person by Designation
  // Designation --  ALL DRIVERS(1)
  //Designation -- ALL RIDERS(2)
  @GetMapping(path = "/api/personByDesignation/{designation}")
  public ResponseEntity<List<Person>> getPersonsByDesignation(@PathVariable(name="designation", required=true)int designation) {
    try {
      ValidationResult validationResult = Validations.isDesignationValid(designation);
      if(validationResult.isValid()==true)
      {
        return ResponseEntity.ok(personService.findByDesignation(designation));
      }
      else
      {
        throw new CrossRideException(validationResult.getMessage(), HttpStatus.BAD_REQUEST);
      }
    }
    catch(Exception ex)
    {
      throw new CrossRideException("An Error Occured", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
}
